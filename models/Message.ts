import * as mongoose from 'mongoose';
import {IMessage} from '../interfaces';

const schema:mongoose.Schema = new mongoose.Schema({
    text: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now()
    },
    from: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    attachmentedPhoto: {
        type: String
    }
});



export default mongoose.model<IMessage>('Message', schema);
