import createError from 'http-errors';
import _logger = require('morgan');
import * as  express from 'express';
import { Request, Response, NextFunction, Router } from "express";
import * as bodyParser from "body-parser";
import * as multer from "multer";
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import mongoose from 'mongoose';
import * as ConnectMongoStore from 'connect-mongo';
import * as session from 'express-session';

import UserRoutes from './routes/users.router';
import config from './config';
import ChatRoutes from './routes/chat.router';

class App {
  public app: express.Application;
  public userRoutes: Router;
  public chatRoutes: Router;

  constructor() {
    this.app = express.default();
    this.userRoutes = new UserRoutes().router;
    this.chatRoutes = new ChatRoutes().router;
    this.config();
  }

  private config(): void {
    mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    
    const MongoStore = ConnectMongoStore.default(session.default);
    

    this.app.use(_logger('dev'));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(cookieParser.default());
    this.app.use(express.static(path.join(__dirname, 'upload')));

    mongoose.set('useCreateIndex', true);
    config.session.store = new MongoStore({
      mongooseConnection: mongoose.connection
    });

    this.app.use('/user', this.userRoutes);
    this.app.use('/chat', this.chatRoutes);


    this.app.use(function(req:Request, res:Response, next:NextFunction) {
      next(createError(404));
    });

    this.app.use(function(err:any, req:Request, res:Response) {
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      res.status(err.status || 500);
      res.render('error');
    });
  }
}

export default App;
