import { Request, Response, NextFunction } from "express";
import { IMessage, IChat, IBaseUser} from '../interfaces';
import {Chat} from "../models/Chat";
import Message from "../models/Message";
import { Socket, Server } from "socket.io";
import * as fs from 'fs';
import atob from 'atob';

function _withSaveImage (socket: Socket, data:any, files:any, cb:() => void) {
    var struct:any = { 
            name: null, 
            type: null, 
            size: 0, 
            data: [], 
            slice: 0, 
        };
    
            
    var binary_string = atob(data.data);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
        

    data.data = bytes.buffer;        
    
    if (!files[data.name]) { 
        files[data.name] = Object.assign({}, struct, data); 
        files[data.name].data = []; 
    }
    
    //convert the ArrayBuffer to Buffer                
    data.data = new Buffer(new Uint8Array(data.data));
    
    
    //save the data 
    files[data.name].data.push(data.data); 
    files[data.name].slice++;               
    
    
    if (files[data.name].slice * 100000 >= files[data.name].size) { 
        //do something with the data 
        var fileBuffer = Buffer.concat(files[data.name].data); 
        const type = files[data.name].type.split("/")[1];
        fs.writeFile(
            `upload/${data.name}.${type}`, 
            fileBuffer, 
            cb
        );
        
        socket.emit('end upload', `${data.name}.${type}`); 
    } else { 
        socket.emit('request slice upload', { 
            currentSlice: files[data.name].slice 
        }); 
    } 
}


export class ChatController {
    normalizeChats = (chats:any, me:any) => {
        return chats.map((chat:any) => {
            return { 
                ...chat.toObject(),
                contact: chat.users.filter((user:any) => user._id.toString() !== me._id.toString())[0] || me }
        })
    }

    // message: string, addressee_id: ObjectId
    send_message = async (req: Request, res: Response, next: NextFunction) => {
        try {
            let chat:IChat = await Chat.getByAddressee(req.body.addressee_id, req.me._id, false)
            
            if (!chat) {
                chat = new Chat({
                    users: [req.body.addressee_id, req.me]
                });
            }
            let message:IMessage = await Message.create({ 
                text: req.body.message, 
                from: req.me,
                date: new Date()
            });
            chat.messages.push(message._id);
            chat.save();
            res.json(message);
        } catch (e) {
            let error:string = "";
            switch (e.name) {
                case "CastError":
                    error = "Некорректные данные";
                    break;
                
                default:
                    error = "Необработанная ошибка";
                    break;
            }
            res.status(400).json({error})
        }
        
        
    }
    // addressee_id?: ObjectId, chat_id?: ObjectId
    get = async (req: Request, res: Response, next: NextFunction) => {
        let {chat_id, addressee_id} = req.query;
        
        if (chat_id || addressee_id) {
            let chat:IChat = await Chat.getByAddressee(req.query.addressee_id, req.me._id, true);
            
            res.json(chat)
        } else {
            let chats:IChat[] = await Chat.find({
                users: { 
                    $in: [req.me] 
                },
                "messages.0": {
                    $exists: true
                }
            }).populate(['users', {path: 'messages', options: { sort: {date: -1}, limit: 1 }}]);
            
            res.json(this.normalizeChats(chats, req.me));
        }
    }
    // addressee_id: ObjectId, page?: number *1 page = 50 messages*
    get_messages = async (req: Request, res: Response, next: NextFunction) => {
        let chat:IChat = await Chat.getByAddressee(req.query.addressee_id, req.me._id, true);
        
        res.json(chat ? chat.messages : []);
    }

    static socketController = (socket:Socket, server:Server) => { 
        
        console.log('connected', socket.handshake.query);

        socket.on('join', async (me_id, addressee_id) => {
            console.log('join data: ', me_id, addressee_id);
            let chat:IChat = await Chat.getByAddressee(addressee_id, me_id, true);

            if (!chat) {
                chat = await Chat.create({users: [addressee_id, me_id]});
            }

            socket.emit('private', chat.messages);

            socket.join(`${chat._id}`);
            const room:string = `${chat._id}`;

            socket.on('chat', async (msg:string, imgName?:string) => {
                let message:IMessage = await Message.create({ 
                    text: msg, 
                    from: me_id,
                    attachmentedPhoto: imgName ? imgName : null,
                    date: new Date()
                });
                chat.messages.push(message._id);
                chat.save();
                server.in(room).emit('chat', message)
            });

            
            let files:any = {};

            socket.on('test', async (data) => {
                _withSaveImage(socket, data, files, () => { 
                    console.log('success!', files);
                    delete files[data.name];
                    console.log(files);
                    
                })
            })
        })
             
        
    }
}