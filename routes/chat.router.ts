import * as express from 'express';
import { AuthController } from "../controllers/auth.controllers";
import { ChatController } from '../controllers/chat.controllers';

// Переписать все с интерфейсами
export default class ChatRoutes {
  public chatController: ChatController;
  public authController: AuthController = new AuthController();

  private _router: express.Router;

  constructor() {
    this.chatController = new ChatController();
    this._router = express.Router();
    this._initRoutes();
  }

  get router(): express.Router {
    return this._router;
  }

  private _initRoutes(): void {
    // message: string, addressee_id: ObjectId, 
    this._router.post('/message/send', this.authController.authenticateJWT, this.chatController.send_message);
    // addressee_id?: ObjectId, chat_id?: ObjectId
    this._router.get('/get', this.authController.authenticateJWT, this.chatController.get);
    // addressee_id: ObjectId, page?: number *1 page = 50 messages*
    this._router.get('/message/get', this.authController.authenticateJWT, this.chatController.get_messages);
    
  }
}
